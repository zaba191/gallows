//
//  CategoryPickerView.swift
//  Gallows
//
//  Created by Zaba on 30.09.2015.
//  Copyright © 2015 Zaba. All rights reserved.
//

import UIKit

enum KindOfPickerView {
    case CategoryGroup
    case Level
}

class GallowPickerView: UIPickerView {
    var kind:KindOfPickerView!

}
