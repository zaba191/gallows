//
//  LetterCollectionViewCell.swift
//  Gallows
//
//  Created by Bartłomiej Zabicki on 16.10.2015.
//  Copyright © 2015 Zaba. All rights reserved.
//

import UIKit

class LetterCollectionViewCell: UICollectionViewCell {
    var letterLabel:UILabel!
    var letterTextField:UITextField!
}
