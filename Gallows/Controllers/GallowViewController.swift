//
//  GallowViewController.swift
//  Gallows
//
//  Created by Zaba on 30.09.2015.
//  Copyright © 2015 Zaba. All rights reserved.
//

import UIKit

class GallowViewController: UIViewController, UITextFieldDelegate, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet var secretWordLabel: UILabel!
    
    var secretWord:String!
    
    @IBOutlet var topImageView: UIImageView!
    @IBOutlet var letterTextField: UITextField!
    @IBOutlet var lettersCollectionView: UICollectionView!
//    @IBOutlet var textFieldView: UIView!
    
    var letterCells:[NSIndexPath] = []
    let levelMistakes:[Int] = [0,14,12,11,11]
    var level:Int!
    var points:Int = 0
    var mistakes:Int = 0
    var selectedTextField:NSIndexPath!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(self.secretWord)
        if self.level == 4 {
            self.letterTextField.enabled = false
            self.letterTextField.hidden = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //Private functions
    
    private func delay(delay:Double, closure:()->()) {
        dispatch_after(
            dispatch_time(
                DISPATCH_TIME_NOW,
                Int64(delay * Double(NSEC_PER_SEC))
            ),
            dispatch_get_main_queue(), closure)
    }
    
    func drawLineFromPoint(start : CGPoint, toPoint end:CGPoint, ofColor lineColor: UIColor, inView view:UIView)  {
        let path = UIBezierPath()
        path.moveToPoint(start)
        path.addLineToPoint(end)
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = path.CGPath
        shapeLayer.strokeColor = lineColor.CGColor
        shapeLayer.lineWidth = 1.0
        view.layer.addSublayer(shapeLayer)
        
        let pathAnimation: CABasicAnimation = CABasicAnimation(keyPath: "strokeEnd")
        pathAnimation.duration = 0.5
        pathAnimation.fromValue = Float(0.0)
        pathAnimation.toValue = Float(1.0)
        
        shapeLayer.addAnimation(pathAnimation, forKey: "strokeEnd")
    }
    
    func drawCircleFromPoint(center:CGPoint, inView view:UIView){
        let bounds = self.topImageView.bounds
        let radius:CGFloat = bounds.height / 15.0
        let rect = CGRectMake(center.x - radius, center.y, radius * 2, radius * 2)
        let path:UIBezierPath = UIBezierPath(ovalInRect: rect)
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = path.CGPath
        shapeLayer.lineWidth = 1.0
        view.layer.addSublayer(shapeLayer)
        
        let pathAnimation: CABasicAnimation = CABasicAnimation(keyPath: "strokeEnd")
        pathAnimation.duration = 0.5
        pathAnimation.fromValue = Float(0.0)
        pathAnimation.toValue = Float(1.0)
        
        shapeLayer.addAnimation(pathAnimation, forKey: "strokeEnd")
    }
    
    private func checkLetter()->Bool{
        var statement:Bool = false
        if self.level != 4 {
            self.secretWord.lowercaseString.characters.enumerate().map { touple in
                if "\(touple.1)" == self.letterTextField.text {
                    statement = true
                    let letterCell = lettersCollectionView.cellForItemAtIndexPath(letterCells[touple.0]) as! LetterCollectionViewCell
                    letterCell.letterLabel.text = self.letterTextField.text
                }
            }
            addLetterToLabel(self.letterTextField.text!)
        }
        else{
            let letterCell = lettersCollectionView.cellForItemAtIndexPath(selectedTextField) as! LetterCollectionViewCell
            if letterCell.letterTextField.text!.lowercaseString == "\(self.secretWord[self.secretWord.startIndex.advancedBy(selectedTextField.row)])".lowercaseString {
                statement = true
                letterCell.userInteractionEnabled = false
                addLetterToLabel(letterCell.letterTextField.text!)
            }
            else{
                addLetterToLabel(letterCell.letterTextField.text!)
                letterCell.letterTextField.text = ""
            }
            
        }
        
        switch statement {
        case true:
            self.points += 1
            self.points == self.secretWord.characters.count ? popUpAlert("Won", message: "You have woooon!") : print("Adding point")
        case false :
            self.mistakes += 1
            drawGallow()
            self.mistakes == self.levelMistakes[self.level] ? popUpAlert("Lose", message: "You have loooose!") : print("Adding mistake")
        }
        
        return statement
    }
    
    private func addLetterToLabel(char:String){
        self.secretWordLabel.text = (self.secretWordLabel.text?.characters.count > 0 ? self.secretWordLabel.text! +  ", " + char : char)
        
        
    }
    
    private func drawGallow(){
        switch self.level {
        case 1 :
            drawEasyGallow()
        case 2 :
            drawNormalGallow()
        case 3 :
            drawHardGallow()
        case 4 :
            //Insane
            drawHardGallow()
        default:
            print("Error")
        }
    }
    
    private func drawEasyGallow(){
        let origin = self.topImageView.bounds.origin
        let bounds = self.topImageView.bounds
        let points:[Int:(CGPoint, CGPoint)] = [
            1 : (from: CGPoint(x: 30, y: origin.y + bounds.height - 10), to: CGPoint(x: 60, y: origin.y + bounds.height - 40)),
            2 : (from: CGPoint(x: 90, y: origin.y + bounds.height - 10), to: CGPoint(x: 60, y: origin.y + bounds.height - 40)),
            3 : (from: CGPoint(x: 60, y: origin.y + bounds.height - 40), to: CGPoint(x: 60, y: origin.y + 20)),
            4 : (from: CGPoint(x: origin.x + bounds.width - 30, y: origin.y + bounds.height - 10), to: CGPoint(x: origin.x + bounds.width - 60, y: origin.y + bounds.height - 40)),
            5 : (from: CGPoint(x: origin.x + bounds.width - 90, y: origin.y + bounds.height - 10), to: CGPoint(x: origin.x + bounds.width - 60, y: origin.y + bounds.height - 40)),
            6 : (from: CGPoint(x: origin.x + bounds.width - 60, y: origin.y + bounds.height - 40), to: CGPoint(x: origin.x + bounds.width - 60, y: origin.y + 20)),
            7 : (from: CGPoint(x: 60, y: origin.y + 20), to: CGPoint(x: origin.x + bounds.width - 60, y: origin.y + 20)),
            8 : (from: CGPoint(x: (origin.x + bounds.width)/2, y: origin.y + 20), to: CGPoint(x: (origin.x + bounds.width)/2, y: origin.y + bounds.height/5)),
            10 : (from: CGPoint(x: bounds.width/2,y: origin.y + bounds.height/5 + bounds.height/7.5), to: CGPoint(x: bounds.width/2, y: origin.y + bounds.height - 100)),
            11 : (from:CGPoint(x: bounds.width/2,y: origin.y + bounds.height/5 + bounds.height/7.5 + 10 ), to:CGPoint(x: bounds.width/2-20,y: origin.y + bounds.height/5 + bounds.height/7.5 + 30)), // Left hand
            12 : (from:CGPoint(x: bounds.width/2,y: origin.y + bounds.height/5 + bounds.height/7.5 + 10), to:CGPoint(x: bounds.width/2+20,y: origin.y + bounds.height/5 + bounds.height/7.5 + 30)), // Right hand
            13 : (from:CGPoint(x: bounds.width/2,y: origin.y + bounds.height - 100), to:CGPoint(x: bounds.width/2-20,y: origin.y + bounds.height - 70)), //Left leg
            14 : (from:CGPoint(x: bounds.width/2,y: origin.y + bounds.height - 100), to:CGPoint(x:bounds.width/2+20,y: origin.y + bounds.height - 70)) //Right leg
        ]
        let circleCenter = CGPoint(x: bounds.width/2, y: origin.y + bounds.height/5)
        switch mistakes {
        case 1,2,3,4,5,6,7,8,10,11,12,13,14:
            self.drawLineFromPoint(points[mistakes]!.0, toPoint: points[mistakes]!.1, ofColor: UIColor.blackColor(), inView: self.topImageView)
        case 9:
            drawCircleFromPoint(circleCenter,inView: self.topImageView)
        default:
            print("Error while drawing a gallow")
        }
    }
    
    private func drawNormalGallow(){
        let origin = self.topImageView.bounds.origin
        let bounds = self.topImageView.bounds
        let points:[Int:(CGPoint, CGPoint)] = [
            1 : (from: CGPoint(x: 50, y: origin.y + bounds.height - 10), to: CGPoint(x: 80, y: origin.y + bounds.height - 40)),
            2 : (from: CGPoint(x: 110, y: origin.y + bounds.height - 10), to: CGPoint(x: 80, y: origin.y + bounds.height - 40)),
            3 : (from: CGPoint(x: 80, y: origin.y + bounds.height - 40), to: CGPoint(x: 80, y: origin.y + 20)),
            4 : (from: CGPoint(x: 80, y: origin.y + 20), to: CGPoint(x: bounds.width/2, y: origin.y + 20)),
            5 : (from: CGPoint(x: 80, y: origin.y + 50), to: CGPoint(x: 110, y: origin.y + 20)),
            6 : (from: CGPoint(x: bounds.width/2, y: origin.y + 20), to: CGPoint(x: bounds.width/2, y: origin.y + bounds.height/5)),
            8 : (from: CGPoint(x: bounds.width/2,y: origin.y + bounds.height/5 + bounds.height/7.5), to: CGPoint(x: bounds.width/2, y: origin.y + bounds.height - 100)),
            9 : (from:CGPoint(x: bounds.width/2,y: origin.y + bounds.height/5 + bounds.height/7.5 + 10 ), to:CGPoint(x: bounds.width/2-20,y: origin.y + bounds.height/5 + bounds.height/7.5 + 30)), // Left hand
            10 : (from:CGPoint(x: bounds.width/2,y: origin.y + bounds.height/5 + bounds.height/7.5 + 10), to:CGPoint(x: bounds.width/2+20,y: origin.y + bounds.height/5 + bounds.height/7.5 + 30)), // Right hand
            11 : (from:CGPoint(x: bounds.width/2,y: origin.y + bounds.height - 100), to:CGPoint(x: bounds.width/2-20,y: origin.y + bounds.height - 70)), //Left leg
            12 : (from:CGPoint(x: bounds.width/2,y: origin.y + bounds.height - 100), to:CGPoint(x:bounds.width/2+20,y: origin.y + bounds.height - 70)) //Right leg
        ]
        let circleCenter = CGPoint(x: bounds.width/2, y: origin.y + bounds.height/5)
        switch mistakes {
        case 1,2,3,4,5,6,8,9,10,11,12:
            self.drawLineFromPoint(points[mistakes]!.0, toPoint: points[mistakes]!.1, ofColor: UIColor.blackColor(), inView: self.topImageView)
        case 7:
            drawCircleFromPoint(circleCenter,inView: self.topImageView)
        default:
            print("Error while drawing a gallow")
        }
    }
    
    private func drawHardGallow(){
        let origin = self.topImageView.bounds.origin
        let bounds = self.topImageView.bounds
        let points:[Int:(CGPoint, CGPoint)] = [
            1 : (from: CGPoint(x: 50,y: origin.y + bounds.height - 10), to:CGPoint(x: bounds.width/7+50,y: origin.y + bounds.height - 10)),
            2 : (from: CGPoint(x: bounds.width/14+50,y: origin.y + bounds.height - 10), to:CGPoint(x: bounds.width/14+50,y: origin.y + 20)),
            3 : (from: CGPoint(x: bounds.width/14+50,y: origin.y + 20), to:CGPoint(x: bounds.width/2+50,y: origin.y + 20)),
            4 : (from:CGPoint(x: bounds.width/14+50,y: origin.y + 50), to:CGPoint(x: bounds.width/14+50+30,y: origin.y + 20)),
            5 : (from: CGPoint(x: bounds.width/2+50,y: origin.y + 20), to:CGPoint(x: bounds.width/2+50,y: origin.y + bounds.height/5)),
            7 : (from:CGPoint(x: bounds.width/2+50,y: origin.y + bounds.height/5 + bounds.height/7.5), to:CGPoint(x: bounds.width/2+50,y: origin.y + bounds.height - 100)),
            8 : (from:CGPoint(x: bounds.width/2+50,y: origin.y + bounds.height/5 + bounds.height/7.5 + 10 ), to:CGPoint(x: bounds.width/2+30,y: origin.y + bounds.height/5 + bounds.height/7.5 + 30)), // Left hand
            9 : (from:CGPoint(x: bounds.width/2+50,y: origin.y + bounds.height/5 + bounds.height/7.5 + 10), to:CGPoint(x: bounds.width/2+70,y: origin.y + bounds.height/5 + bounds.height/7.5 + 30)), // Right hand
            10 : (from:CGPoint(x: bounds.width/2+50,y: origin.y + bounds.height - 100), to:CGPoint(x: bounds.width/2+30,y: origin.y + bounds.height - 70)), //Left leg
            11 : (from:CGPoint(x: bounds.width/2+50,y: origin.y + bounds.height - 100), to:CGPoint(x:bounds.width/2+70,y: origin.y + bounds.height - 70)) //Right leg
        ]
        let circleCenter = CGPoint(x: bounds.width/2+50, y: origin.y + bounds.height/5)
        switch mistakes {
        case 1,2,3,4,5,7,8,9,10,11:
            self.drawLineFromPoint(points[mistakes]!.0, toPoint: points[mistakes]!.1, ofColor: UIColor.blackColor(), inView: self.topImageView)
        case 6:
            drawCircleFromPoint(circleCenter,inView: self.topImageView)
        default:
            print("Error while drawing a gallow")
        }
    }
    
    func popUpAlert(title:String,message:String){
        let alert = UIAlertController(
            title: title,
            message: message,
            preferredStyle: UIAlertControllerStyle.Alert
        )
        alert.addAction(UIAlertAction(
            title: "Ok",
            style: UIAlertActionStyle.Default,
            handler: { (UIAlertAction) -> Void in
                if let navController = self.navigationController {
                    navController.popViewControllerAnimated(true)
                }
            }
            ))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    //MARK: - UITextFieldDelegate
    
    func textFieldDidBeginEditing(textField: UITextField) {
        if self.level == 4{
            let cell = textField.superview as! LetterCollectionViewCell
            self.selectedTextField = lettersCollectionView.indexPathForCell(cell)
        }
        animateViewMoving(true, moveValue: 210)
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        animateViewMoving(false, moveValue: 210)
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        let newLength = textField.text!.utf16.count + string.utf16.count - range.length
        return newLength <= 1
    }
    
    func animateViewMoving (up:Bool, moveValue :CGFloat){
        let movementDuration:NSTimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = CGRectOffset(self.view.frame, 0,  movement)
        UIView.commitAnimations()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        if textField.text != "" && textField.text != " "{
            delay(0.4, closure: { () -> () in
                if self.level == 4 {
                    self.checkLetter()
                }
                else{
                    var statement:Bool = false
                    self.secretWordLabel.text!.characters.enumerate().map { touple in
                        if "\(touple.1)" == self.letterTextField.text! {
                            textField.text = ""
                            statement = true
                        }
                    }
                    guard statement == false else {
                        return
                    }
                    self.checkLetter()
                    textField.text = ""
                }
            })
        }
        return true
    }
    
    //MARK: - UICollectionViewDataSource
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        self.letterCells.append(indexPath)
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("singleLetterCell", forIndexPath: indexPath) as! LetterCollectionViewCell
        if self.level != 4{
            let letterLabel = UILabel(frame: CGRect(origin: CGPoint(x: 0.0, y: 0.0), size: CGSize(width: cell.frame.width, height: cell.frame.width-5)))
            letterLabel.textAlignment = .Center
            letterLabel.font.fontWithSize((22))
            cell.letterLabel = letterLabel
            cell.addSubview(cell.letterLabel)
        }
        else{
            let letterTextField = UITextField(frame: CGRect(origin: CGPoint(x: 0.0, y: 0.0), size: CGSize(width: cell.frame.width, height: cell.frame.width-5)))
            letterTextField.textAlignment = .Center
            letterTextField.placeholder = "-"
            letterTextField.font!.fontWithSize((22))
            letterTextField.delegate = self
            cell.letterTextField = letterTextField
            cell.addSubview(cell.letterTextField)
        }
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.secretWord.characters.count
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        let viewWidth = collectionView.frame.width
        let totalCellWidth = 50 * self.secretWord.characters.count
        let totalSpacingWidth = 10 * (self.secretWord.characters.count - 1)
        
        let leftInset = (viewWidth - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
        let rightInset = leftInset
        
        return UIEdgeInsetsMake(0, leftInset, 0, rightInset)
    }
    
    
}
