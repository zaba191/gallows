//
//  ViewController.swift
//  Gallows
//
//  Created by Zaba on 30.09.2015.
//  Copyright © 2015 Zaba. All rights reserved.
//

import Parse
import UIKit

class LevelViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {

    //MARK: - Outlets
    @IBOutlet var categoryGroupPickerView: GallowPickerView!
    @IBOutlet var levelPickerView: GallowPickerView!
    
    
    //MARK: - Properties
    
    var categoryData:[String] = []
    var groupData:[[String]] = []
    let levelData:[String] = ["Easy", "Normal", "Hard", "Insane"]
    
//    let answersData:[(group:String, answers:[String])] = [
//        ("Plants", ["Roza","Bratek"]),
//        ("Anilams")
//    ]
    var answerData:[String: [String]] = [
        "Plants" : [],
        "Animals" : [],
        "Trees" : [],
        "Functions" : [],
        "Tryg" : [],
        "Geometry" : [],
        "Shapes" : [],
        "Programming" : [],
        "Mobiles" : []
    ]
    
    
    var selectedCategory:Int = 0
    var selectedGroup:Int = 0
    var selectedLevel:Int = 1
    
    //MARK: - Lifecycle
    
    override func viewWillAppear(animated: Bool) {
        self.downlaodCategories()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.downloadAnswers()
        self.categoryGroupPickerView.kind    = KindOfPickerView.CategoryGroup
        self.levelPickerView.kind       = KindOfPickerView.Level
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - UIPickerViewDataSource
    
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        if let picker = pickerView as? GallowPickerView {
            if picker.kind == KindOfPickerView.CategoryGroup {
                return 2
            }
            return 1
        }
        return 0
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if let picker = pickerView as? GallowPickerView {
            if picker.kind == KindOfPickerView.CategoryGroup {
                if component == 0 {
                    return categoryData.count
                }
                else if component == 1 {
                    return groupData[selectedCategory].count
                }
            }
            else if picker.kind == KindOfPickerView.Level {
                return self.levelData.count
            }
            
        }
        return 0
    }
    
    //MARK: - UIPickerViewDelegate
    
    
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if let picker = pickerView as? GallowPickerView {
            if picker.kind == KindOfPickerView.CategoryGroup{
                if component == 0 {
                    return categoryData[row]
                }
                else if component == 1 {
                    let groupCount = groupData[self.selectedCategory].count-1
                    if row > groupCount{
                        return groupData[self.selectedCategory][groupCount]
                    }
                    return groupData[selectedCategory][row]
                }
            }
            else if picker.kind == KindOfPickerView.Level {
                return levelData[row]
            }
            
        }
        return "Empty"
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if let picker = pickerView as? GallowPickerView {
            if picker.kind == KindOfPickerView.CategoryGroup{
                if component == 0 {
                    self.selectedCategory = row
                    self.categoryGroupPickerView.reloadAllComponents()
                    self.categoryGroupPickerView.selectRow(0, inComponent: 1, animated: true)
                    self.selectedGroup = self.categoryGroupPickerView.selectedRowInComponent(1)
                    
                    
                }
                else if component == 1 {
                    self.selectedGroup = row
                }
            }
            else if picker.kind == KindOfPickerView.Level {
                self.selectedLevel = row+1
            }
            
            
        }
    }
    
    //MARK: - SegueMethods
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let destination = segue.destinationViewController as? GallowViewController{
            destination.secretWord = getRandomAnswer(groupData[selectedCategory][self.selectedGroup])
            destination.level = self.selectedLevel
        }
    }

    //MARK: - Private functions
    
    private func getRandomAnswer(group:String) -> String {
        let count = self.answerData[group]!.count
        let randomIndex = Int(arc4random_uniform(UInt32(count)))
        return (self.answerData[group])![randomIndex]
    }
    
    private func downlaodCategories(){
        let jsonUrl = "http://gallow.hol.es?toDownload=Categories"
        
        let session = NSURLSession.sharedSession()
        let shotsUrl = NSURL(string: jsonUrl)
        
        let task = session.dataTaskWithURL(shotsUrl!) {
            (data, response, error) -> Void in
            
            do {
                let jsonData = try NSJSONSerialization.JSONObjectWithData(data!, options:NSJSONReadingOptions.MutableContainers )
                
                
                self.categoryData = jsonData as! [String]
                self.categoryGroupPickerView.reloadComponent(0)
                self.categoryGroupPickerView.selectRow(1, inComponent: 0, animated: false)
                self.categoryGroupPickerView.selectRow(0, inComponent: 0, animated: true)
                self.downlaodGroups()
            } catch  {
                // Error
                print("Error")
            }
        }
        task.resume()
    }
    
    private func downlaodGroups(){
        for var i=1; i <= self.categoryData.count; i += 1{
            print(i)
            let jsonUrl = "http://gallow.hol.es?toDownload=Groups&categoryId=\(i)"
            
            let session = NSURLSession.sharedSession()
            let shotsUrl = NSURL(string: jsonUrl)
            
            let task = session.dataTaskWithURL(shotsUrl!) {
                (data, response, error) -> Void in
                
                do {
                    let jsonData = try NSJSONSerialization.JSONObjectWithData(data!, options:NSJSONReadingOptions.MutableContainers )
                    
                    
                    self.categoryGroupPickerView.reloadAllComponents()
                    print(jsonData as! [String])
                    self.groupData.append(jsonData as! [String])
                    print(self.groupData)
                } catch  {
                    // Error
                    print("Error")
                }
            }
            task.resume()
        }
        self.categoryGroupPickerView.reloadComponent(1)
        self.categoryGroupPickerView.selectRow(1, inComponent: 1, animated: false)
        self.categoryGroupPickerView.selectRow(0, inComponent: 1, animated: true)
        self.categoryGroupPickerView.reloadAllComponents()
    }

    
    private func downloadAnswers(){
        self.groupData.map { (group) -> Void in
            group.map { (word) -> Void in
                var query = PFQuery(className:"answers")
                query.whereKey("group", equalTo: word)
                query.findObjectsInBackgroundWithBlock {
                    (objects: [PFObject]?, error: NSError?) -> Void in
                    
                    if error == nil {
                        // The find succeeded.
                        print("Successfully retrieved \(objects!.count) scores.")
                        // Do something with the found objects
                        if let objects = objects as [PFObject]! {
                            for object in objects {
                                self.answerData[word]?.append(object["answer"] as! (String))
                            }
                        }
                    } else {
                        // Log details of the failure
                        print("Error: \(error!) \(error!.userInfo)")
                    }
                }         }
        }
    }
    
}
